module gitlab.com/witchbrew/go/jsonschema

go 1.14

require (
	github.com/santhosh-tekuri/jsonschema v1.2.4
	github.com/stretchr/testify v1.6.1
)
