package jsonschema

import (
	"encoding/json"
	"fmt"
	"github.com/santhosh-tekuri/jsonschema"
	"strings"
)

type Schema struct {
	schema *jsonschema.Schema
}

type (
	Obj = map[string]interface{}
	Arr = []interface{}
	Sch map[string]interface{}
)

func mustCompile(schema string) *jsonschema.Schema {
	compiler := jsonschema.NewCompiler()
	if err := compiler.AddResource("", strings.NewReader(schema)); err != nil {
		panic(err)
	}
	jsonSchema, err := compiler.Compile("")
	if err != nil {
		panic(err)
	}
	return jsonSchema
}

func (s Sch) MustCompile() *Schema {
	schemaData, err := json.Marshal(s)
	if err != nil {
		panic(err)
	}
	return &Schema{mustCompile(string(schemaData))}
}

func instancePtrToPath(ptr string) []string {
	parts := strings.Split(ptr, "/")
	if parts[0] != "#" {
		panic("root element of validated instance != #")
	}
	return parts[1:]
}

type PathError struct {
	Path    []string
	Message string
}

type PathErrorBuilder []string

func ErrorForPath(path ...string) PathErrorBuilder {
	if path == nil {
		path = make([]string, 0)
	}
	return path
}

func (pe PathErrorBuilder) WithMsg(msg string) *PathError {
	return &PathError{
		Path:    pe,
		Message: msg,
	}
}

func (pe PathErrorBuilder) WithMsgf(format string, args ...interface{}) *PathError {
	return pe.WithMsg(fmt.Sprintf(format, args...))
}

func (e *PathError) StringPath(separator string) string {
	path := append([]string{"@root"}, e.Path...)
	return strings.Join(path, separator)
}

func (e *PathError) Error() string {
	path := e.StringPath(".")
	return fmt.Sprintf(`%s: %s`, path, e.Message)
}

func flattenErr(err *jsonschema.ValidationError) []*PathError {
	var pathErrors []*PathError
	if err.Causes == nil {
		var message string
		if strings.HasPrefix(err.Message, "additionalProperties") {
			message = strings.Replace(err.Message, "additionalProperties", "additional property", 1)
		} else {
			message = err.Message
		}
		pathErrors = append(pathErrors, &PathError{
			Path:    instancePtrToPath(err.InstancePtr),
			Message: message,
		})
	} else {
		for _, cause := range err.Causes {
			childErrors := flattenErr(cause)
			for _, childError := range childErrors {
				pathErrors = append(pathErrors, childError)
			}
		}
	}
	return pathErrors
}

type ValidationError struct {
	Errors []*PathError
}

func (e *ValidationError) Error() string {
	errStrings := make([]string, len(e.Errors))
	for index, err := range e.Errors {
		errStrings[index] = err.Error()
	}
	return fmt.Sprintf("validation error: [%s]", strings.Join(errStrings, ", "))
}

func (s *Schema) Validate(i interface{}) *ValidationError {
	err := s.schema.ValidateInterface(i)
	if err != nil {
		return &ValidationError{flattenErr(err.(*jsonschema.ValidationError))}
	}
	return nil
}

func NewValidationError(pathError *PathError, pathErrors ...*PathError) *ValidationError {
	errs := make([]*PathError, len(pathErrors)+1)
	errs[0] = pathError
	for index, pErr := range pathErrors {
		errs[index+1] = pErr
	}
	return &ValidationError{Errors: errs}
}
