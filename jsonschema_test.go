package jsonschema

import (
	"encoding/json"
	"errors"
	"github.com/santhosh-tekuri/jsonschema"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestPositive(t *testing.T) {
	s := Sch{
		"type": "object",
		"properties": Obj{
			"string": Obj{
				"type": "string",
			},
		},
	}.MustCompile()
	validationErr := s.Validate(Obj{"string": "hello world"})
	require.Nil(t, validationErr)
}

func TestIntInsteadOfStr(t *testing.T) {
	s := Sch{"type": "string"}.MustCompile()
	validationErr := s.Validate(42)
	require.NotNil(t, validationErr)
	require.Equal(
		t,
		`validation error: [@root: expected string, but got number]`,
		validationErr.Error(),
	)
}

func TestUnknownProperties(t *testing.T) {
	s := Sch{
		"type":                 "object",
		"additionalProperties": false,
	}.MustCompile()
	validationErr := s.Validate(Obj{"unknown": "hello world"})
	require.NotNil(t, validationErr)
	require.Equal(
		t,
		`validation error: [@root: additional property "unknown" not allowed]`,
		validationErr.Error(),
	)
}

func TestBadArrayElement(t *testing.T) {
	s := Sch{
		"type": "array",
		"items": Obj{
			"type": "string",
		},
	}.MustCompile()
	validationErr := s.Validate([]interface{}{"hello", 2})
	require.NotNil(t, validationErr)
	require.Equal(
		t,
		`validation error: [@root.1: expected string, but got number]`,
		validationErr.Error(),
	)
}

func TestFlattenError(t *testing.T) {
	defer func() {
		r := recover()
		require.NotNil(t, r)
		require.Equal(t, "root element of validated instance != #", r)
	}()
	flattenErr(&jsonschema.ValidationError{InstancePtr: ""})
}

type errJSONstruct struct{}

func (s *errJSONstruct) MarshalJSON() ([]byte, error) {
	return nil, errors.New("bad JSON")
}

func TestSchJSONMarshalErr(t *testing.T) {
	defer func() {
		r := recover()
		require.NotNil(t, r)
		require.Equal(t, "bad JSON", r.(*json.MarshalerError).Unwrap().Error())
	}()
	Sch{"key": &errJSONstruct{}}.MustCompile()
}

func TestBadJSONSchema(t *testing.T) {
	defer func() {
		r := recover()
		require.NotNil(t, r)
		require.Contains(t, r.(error).Error(), "compilation failed")
	}()
	Sch{"type": "non existent type"}.MustCompile()
}

func TestMustCompileInternalPanic(t *testing.T) {
	defer func() {
		r := recover()
		require.NotNil(t, r)
		require.Contains(t, r.(error).Error(), "invalid character 'm' looking for beginning of value")
	}()
	mustCompile("malformed JSON")
}

func TestNewValidationError(t *testing.T) {
	onePathErr := ErrorForPath("root", "one").WithMsg("error")
	twoPathErr := ErrorForPath("root", "two").WithMsg("error")
	valErr := NewValidationError(onePathErr)
	require.NotNil(t, valErr)
	require.Equal(t, []*PathError{onePathErr}, valErr.Errors)
	valErr = NewValidationError(onePathErr, twoPathErr)
	require.NotNil(t, valErr)
	require.Equal(t, []*PathError{onePathErr, twoPathErr}, valErr.Errors)
}
